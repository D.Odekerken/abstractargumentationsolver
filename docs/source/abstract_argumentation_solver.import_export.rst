abstract\_argumentation\_solver.import\_export package
======================================================

Submodules
----------

abstract\_argumentation\_solver.import\_export.read\_argumentation\_framework\_aspartix module
----------------------------------------------------------------------------------------------

.. automodule:: abstract_argumentation_solver.import_export.read_argumentation_framework_aspartix
   :members:
   :undoc-members:
   :show-inheritance:

abstract\_argumentation\_solver.import\_export.write\_extension module
----------------------------------------------------------------------

.. automodule:: abstract_argumentation_solver.import_export.write_extension
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: abstract_argumentation_solver.import_export
   :members:
   :undoc-members:
   :show-inheritance:
