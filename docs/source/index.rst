.. abstract_argumentation_solver documentation master file, created by
   sphinx-quickstart on Wed Jul  8 08:13:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Abstract Argumentation Solver
=============================

Example use

.. code-block:: python

   from abstract_argumentation_solver.cls.label import Label
   from abstract_argumentation_solver.import_export.read_argumentation_framework_aspartix import read_argumentation_framework_aspartix
   from abstract_argumentation_solver.import_export.write_extension import write_extension
   from abstract_argumentation_solver.labeling.grounded_extension_labeler import GroundedExtensionLabeler

   af = read_argumentation_framework_aspartix('example_af.apx')
   labeler = GroundedExtensionLabeler()
   labels = labeler.get_labels(af)
   grounded_extension = labeler.get_extension(af)
   write_extension(grounded_extension, 'result_path.txt')


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
