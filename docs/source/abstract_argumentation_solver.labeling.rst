abstract\_argumentation\_solver.labeling package
================================================

Submodules
----------

abstract\_argumentation\_solver.labeling.grounded\_extension\_labeler module
----------------------------------------------------------------------------

.. automodule:: abstract_argumentation_solver.labeling.grounded_extension_labeler
   :members:
   :undoc-members:
   :show-inheritance:

abstract\_argumentation\_solver.labeling.labeler\_interface module
------------------------------------------------------------------

.. automodule:: abstract_argumentation_solver.labeling.labeler_interface
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: abstract_argumentation_solver.labeling
   :members:
   :undoc-members:
   :show-inheritance:
