# Abstract Argumentation Solver
This is a solver for abstract argumentation.
It currently only labels arguments by grounded semantics.

## Example use
```python
from abstract_argumentation_solver.cls.label import Label
from abstract_argumentation_solver.import_export.read_argumentation_framework_aspartix import read_argumentation_framework_aspartix
from abstract_argumentation_solver.import_export.write_extension import write_extension
from abstract_argumentation_solver.labeling.grounded_extension_labeler import GroundedExtensionLabeler

af = read_argumentation_framework_aspartix('example_af.apx')
labeler = GroundedExtensionLabeler()
labels = labeler.get_labels(af)
grounded_extension = labeler.get_extension(af)
write_extension(grounded_extension, 'result_path.txt')
```