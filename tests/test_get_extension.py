import unittest

from abstract_argumentation_solver.import_export.read_argumentation_framework_aspartix import \
    read_argumentation_framework_aspartix
from abstract_argumentation_solver.labeling.grounded_extension_labeler import GroundedExtensionLabeler


class TestGetExtension(unittest.TestCase):
    def test_grounded_extension_labeler_non_circular(self):
        af = read_argumentation_framework_aspartix('../data/example_af.apx')
        labeler = GroundedExtensionLabeler()
        grounded_extension = labeler.get_extension(af)
        self.assertTrue(af.arguments['a1'] in grounded_extension)
        self.assertFalse(af.arguments['a2'] in grounded_extension)
        self.assertTrue(af.arguments['a3'] in grounded_extension)

    def test_grounded_extension_labeler_circular(self):
        af = read_argumentation_framework_aspartix('../data/circular_af.apx')
        labeler = GroundedExtensionLabeler()
        grounded_extension = labeler.get_extension(af)
        self.assertFalse(af.arguments['a1'] in grounded_extension)
        self.assertFalse(af.arguments['a2'] in grounded_extension)
        self.assertFalse(af.arguments['a3'] in grounded_extension)


if __name__ == '__main__':
    unittest.main()
