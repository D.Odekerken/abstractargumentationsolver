import unittest

from abstract_argumentation_solver.import_export.read_argumentation_framework_aspartix import \
    read_argumentation_framework_aspartix


class TestImport(unittest.TestCase):
    def test_import(self):
        af = read_argumentation_framework_aspartix('../data/example_af.apx')
        self.assertEqual(len(af.arguments), 3)


if __name__ == '__main__':
    unittest.main()