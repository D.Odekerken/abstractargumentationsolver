import unittest

from abstract_argumentation_solver.cls.label import Label
from abstract_argumentation_solver.import_export.read_argumentation_framework_aspartix import \
    read_argumentation_framework_aspartix
from abstract_argumentation_solver.labeling.grounded_extension_labeler import GroundedExtensionLabeler


class TestImport(unittest.TestCase):
    def test_grounded_extension_labeler_non_circular(self):
        af = read_argumentation_framework_aspartix('../data/example_af.apx')
        labeler = GroundedExtensionLabeler()
        labels = labeler.get_labels(af)
        self.assertTrue(labels[af.arguments['a1']] == Label.IN)
        self.assertTrue(labels[af.arguments['a2']] == Label.OUT)
        self.assertTrue(labels[af.arguments['a3']] == Label.IN)

    def test_grounded_extension_labeler_circular(self):
        af = read_argumentation_framework_aspartix('../data/circular_af.apx')
        labeler = GroundedExtensionLabeler()
        labels = labeler.get_labels(af)
        self.assertTrue(labels[af.arguments['a1']] == Label.UNDECIDED)
        self.assertTrue(labels[af.arguments['a2']] == Label.UNDECIDED)
        self.assertTrue(labels[af.arguments['a3']] == Label.UNDECIDED)


if __name__ == '__main__':
    unittest.main()