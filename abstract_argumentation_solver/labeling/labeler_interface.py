from typing import Dict, List

from abstract_argumentation_solver.cls.argument import Argument
from abstract_argumentation_solver.cls.argumentation_framework import ArgumentationFramework
from abstract_argumentation_solver.cls.label import Label


class LabelerInterface:
    def get_labels(self, argumentation_framework: ArgumentationFramework) -> Dict[Argument, Label]:
        """
        Compute the labels of the arguments for a given argumentation framework. This methods is implemented in the
        concrete instantiations.

        :param argumentation_framework: Argumentation framework for which the labels should be computed.
        :return: Label for each Argument in the argumentation framework.
        """
        pass

    def get_extension(self, argumentation_framework: ArgumentationFramework) -> List[Argument]:
        """
        Compute the extension (i.e. the IN arguments) for a given argumentation framework.

        :param argumentation_framework: Argumentation framework for which the extension should be computed.
        :return: Set of Arguments in the extension of the argumentation framework.
        """
        labels = self.get_labels(argumentation_framework)
        return [argument for argument, label in labels.items() if label == Label.IN]
