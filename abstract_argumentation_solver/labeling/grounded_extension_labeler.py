from typing import Dict

from abstract_argumentation_solver.cls.argument import Argument
from abstract_argumentation_solver.cls.argumentation_framework import ArgumentationFramework
from abstract_argumentation_solver.cls.label import Label
from abstract_argumentation_solver.labeling.labeler_interface import LabelerInterface


class GroundedExtensionLabeler(LabelerInterface):
    def get_labels(self, argumentation_framework: ArgumentationFramework) -> Dict[Argument, Label]:
        """
        Compute the labels of the arguments for a given argumentation framework, based on grounded semantics.

        :param argumentation_framework: Argumentation framework for which the labels should be computed.
        :return: Label for each Argument in the argumentation framework.
        """
        labels = {argument: Label.UNDECIDED for argument in argumentation_framework.arguments.values()}

        todo_round = [argument for argument in argumentation_framework.arguments.values() if argument.attacked_by == []]

        while todo_round:
            todo_new_round = []
            for argument in todo_round:
                if any([labels[attacker] == Label.IN for attacker in argument.attacked_by]):
                    labels[argument] = Label.OUT
                elif all([labels[attacker] == Label.OUT for attacker in argument.attacked_by]):
                    labels[argument] = Label.IN
                if labels[argument] in [Label.IN, Label.OUT]:
                    todo_new_round += [attacked for attacked in argument.is_attacking
                                       if labels[attacked] == Label.UNDECIDED]
            todo_round = todo_new_round
        return labels
