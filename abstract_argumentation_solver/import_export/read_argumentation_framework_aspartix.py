from abstract_argumentation_solver.cls.argument import Argument
from abstract_argumentation_solver.cls.argumentation_framework import ArgumentationFramework


def read_argumentation_framework_aspartix(file_path: str) -> ArgumentationFramework:
    """
    Read an ArgumentationFramework from a file in the Aspartix format.
    
    :param file_path: Path to file in the Aspartix format.
    :return: Argumentation framework.
    """
    argumentation_framework = ArgumentationFramework()
    with open(file_path, 'r') as read_file:
        lines = read_file.readlines()
        for line in lines:
            if line.startswith('arg'):
                argument_name = line[line.index('(') + 1: line.index(')')]
                new_argument = Argument(argument_name)
                argumentation_framework.add_argument(new_argument)
            elif line.startswith('att'):
                attacker = argumentation_framework.arguments[line[line.index('(') + 1: line.index(',')]]
                attacked = argumentation_framework.arguments[line[line.index(',') + 1: line.index(')')]]
                attacked.add_attack_by(attacker)

    return argumentation_framework
