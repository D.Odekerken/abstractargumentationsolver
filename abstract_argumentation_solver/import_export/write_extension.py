from typing import List

from abstract_argumentation_solver.cls.argument import Argument


def write_extension(extension: List[Argument], write_path: str) -> None:
    """
    Write extension to a file.

    :param extension: Extension (set of Arguments)
    :param write_path: Path to write to
    """
    with open(write_path, 'w') as write_file:
        write_file.writelines([str(argument) for argument in extension])
