from enum import Enum


class Label(Enum):
    UNDECIDED = 1
    IN = 2
    OUT = 3
