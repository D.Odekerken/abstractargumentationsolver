from typing import Dict

from abstract_argumentation_solver.cls.argument import Argument


class ArgumentationFramework:
    def __init__(self):
        self.arguments: Dict[str, Argument] = {}

    def add_argument(self, argument: Argument):
        self.arguments[argument.name] = argument
