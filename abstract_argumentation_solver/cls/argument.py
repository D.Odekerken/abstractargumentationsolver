class Argument:
    def __init__(self, argument_name: str):
        self.name = argument_name
        self.attacked_by = []
        self.is_attacking = []

    def __str__(self):
        return '{} (Attacks {} and is attacked by {})'.format(
            self.name,
            ', '.join([attacked.name for attacked in self.is_attacking]),
            ', '.join([attacker.name for attacker in self.attacked_by])
        )

    def add_attack_by(self, other: 'Argument'):
        self.attacked_by.append(other)
        other.is_attacking.append(self)
